import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import auth from '../../firebase'

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: null,
      password: null,
      currentUser: null,
      message: null
    }
  }
  userNameInputChanged = (event) => {
    this.setState({
      userName: event.target.value
    })
  }
  passwordInputChanged = (event) => {
    this.setState({
      password: event.target.value
    })
  }
  logInButtonClicked = (event) => {
    event.preventDefault()
    auth
    .signInWithEmailAndPassword(this.state.userName, this.state.password)
    .then(response => {
      this.setState({
        currentUser: response.user
      })
      console.log(response.user.providerData)
    })
    .catch(error => {
      this.setState({
        message: error.message
      })
    })
  }
  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input onChange={this.userNameInputChanged} type="text" placeholder="Username" autoComplete="username" />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input onChange={this.passwordInputChanged} type="password" placeholder="Password" autoComplete="current-password" />
                      </InputGroup>
                      {this.state.message ?<p className="alert alert-danger">{this.state.message}</p>:<div></div>}
                      <Row>
                        <Col xs="6">
                          <Button type="submit" onClick={this.logInButtonClicked} color="primary" className="px-4">Login</Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">Forgot password?</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{
        width: '44%'
      }}>
                  <CardBody className="text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                      <Link to="/register">
                        <Button color="primary" className="mt-3" active tabIndex={-1}>Register Now!</Button>
                      </Link>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
      );
  }
}

export default Login;
